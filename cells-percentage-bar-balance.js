(function() {

  'use strict';

  Polymer({

    is: 'cells-percentage-bar-balance',

    behaviors: [
      Polymer.i18nBehavior
    ],

    properties: {
      /*
      * Min balance to percentage bar, used cells-atom-amount
      * @type {Object}
      * @public
      */
      minBalance: {
        type: Object,
        value: {
          amount: 0,
          currency: 'USD'
        }
      },
      /*
      * Max balance to percentage bar, used cells-atom-amount
      * @type {Object}
      * @public
      */
      maxBalance: {
        type: Object,
        value: {
          amount: 0,
          currency: 'USD'
        }
      },
      /*
      * Available balance to percentage bar, used cells-atom-amount
      * @type {Object}
      * @public
      */
      availableBalance: {
        type: Object
      },
      /*
      * Posted balance to percentage bar, used cells-atom-amount
      * @type {Object}
      * @public
      */
      postedBalance: {
        type: Object
      },
      /*
      * Transit balance to percentage bar, used cells-atom-amount
      * @type {Object}
      * @public
      */
      transitBalance: {
        type: Object
      },
      /*
      * Label Left of posted Balance
      * @type {String}
      * @public
      */
      leftLabel: {
        type: String
      },
      /*
      * Label Right of posted Balance
      * @type {String}
      * @public
      */
      rightLabel: {
        type: String
      },
      /*
      * Label Center of transit Balance
      * @type {String}
      * @public
      */
      centerLabel: {
        type: String
      }
    }
  });
}());
