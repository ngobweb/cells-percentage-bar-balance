var templateBind = Polymer.dom(this.root).querySelector('[is=dom-bind]');
var mock = {
	availableBalance: {
		amount: 12000,
		currency: 'USD'
	},
	postedBalance: {
		amount: 18000,
		currency: 'USD'
	},
	transitBalance: {
		amount: 6000,
		currency: 'USD'
	},
	maxBalance: {
		amount: 36000,
		currency: 'USD'
	}
};

// The dom-change event signifies when the template has stamped its DOM.
templateBind.addEventListener('dom-change', function() {
  // auto binding template is ready
  templateBind.set('greeting', 'Try declarative!');
  templateBind.set('mock', mock);
});

document.addEventListener('WebComponentsReady', function() {
  var myEl = document.querySelector('#myEl');
	myEl.set('leftLabel', myEl.t('cells-percentage-bar-balance-posted-balance', myEl.lang));
  myEl.set('rightLabel', myEl.t('cells-percentage-bar-balance-available-balance', myEl.lang));
  myEl.set('centerLabel', myEl.t('cells-percentage-bar-balance-in-transit', myEl.lang));
});
