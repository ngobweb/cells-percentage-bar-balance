# cells-percentage-bar-balance

Your component description.

Example:
```html
<cells-percentage-bar-balance></cells-percentage-bar-balance>
```

## Data model
[comment]: <> (use either one of the data model docs examples)

Canonical DM: 'entityName', example available [List of entities]

Component own DM example:

```json
{}
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-percentage-bar-balance-scope      | scope description | default value  |
| --cells-percentage-bar-balance  | empty mixin     | {}             |
